/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testtvmain;

import java.io.File;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 *
 * @author office
 */
public class FilesTreeModel implements TreeModel {
	private TreeModelListener listener;
	
	private FileTreeNode root;
	
	public FilesTreeModel(File file) {
		root = new FileTreeNode(file);
	}
	
	public void scan(File file) {
		root = new FileTreeNode(file);
		
		root.scan();
		if(listener != null) {
			TreeModelEvent event = new TreeModelEvent(this, new TreePath(root));
			listener.treeStructureChanged(event);
		}
	}
	
	@Override
	public Object getRoot() {
		return root;
	}

	@Override
	public Object getChild(Object parent, int index) {
		return ((FileTreeNode)parent).getChildAt(index);
	}

	@Override
	public int getChildCount(Object parent) {
		return ((FileTreeNode)parent).getChildCount();
	}

	@Override
	public boolean isLeaf(Object node) {
		return ((FileTreeNode)node).isLeaf();
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		return ((FileTreeNode)parent).getIndex((FileTreeNode)child);
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		this.listener = l;
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		if(this.listener == l) {
			this.listener = null;
		}
	}
	
}
