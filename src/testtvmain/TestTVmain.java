/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testtvmain;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.ExpandVetoException;

/**
 *
 * @author office
 */
public class TestTVmain extends JFrame {
	
	private JTextField rootPathTextField;
	private JButton showButton;
	
	private JTree filesTree;

	public TestTVmain() throws HeadlessException {
		super("TestTVmain");
		
		File file = new File(System.getProperty("user.home"));
		
		rootPathTextField = new JTextField(30);
		showButton = new JButton("Scan");
		filesTree = new JTree(new FilesTreeModel(file));
		
		createGUI();
	}
	
	private void createGUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new GridBagLayout());
		
//		rootPathTextField.setEditable(false);
		
		File file = ((FileTreeNode)((FilesTreeModel)filesTree.getModel()).getRoot()).getFile();
		rootPathTextField.setText(file.getAbsolutePath());
		
		showButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {			
				((FilesTreeModel)filesTree.getModel()).scan(new File(rootPathTextField.getText().trim()));
				expandAllNodes();
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(filesTree);
		
		filesTree.addTreeWillExpandListener(new TreeWillExpandListener() {
			@Override
			public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
//				((FileTreeNode)event.getPath().getLastPathComponent()).scan();
			}

			@Override
			public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
				
			}
		});
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0;
		c.insets = new Insets(0, 0, 5, 5);
		c.fill = GridBagConstraints.HORIZONTAL;
		add(rootPathTextField, c);
		
		c.gridx++;
		c.weightx = 0.0;
		c.insets = new Insets(0, 0, 5, 0);
		add(showButton, c);
		
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 2;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(0, 0, 0, 0);
		add(scrollPane, c);
		
		pack();
		setLocationRelativeTo(getParent());
	}
	
	private void expandAllNodes() {
		SwingWorker worker = new SwingWorker() {
			@Override
			protected Object doInBackground() throws Exception {
				for(int i = 0; i < filesTree.getRowCount(); i++) {
					filesTree.expandRow(i);
				}
				
				return null;
			}

			@Override
			protected void done() {
				showButton.setEnabled(true);
			}
		};
		
		showButton.setEnabled(false);
		worker.execute();
	}
	

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TestTVmain().setVisible(true);
			}
		});
	}
}
