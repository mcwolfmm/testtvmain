/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testtvmain;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.swing.tree.TreeNode;

/**
 *
 * @author office
 */
public class FileTreeNode implements TreeNode {
	private File file;
	
	private List<FileTreeNode> files;

	public FileTreeNode(File file) {
		this.file = file;
		files = new ArrayList<>();
	}
	
	public void scan() {
		if(file == null) {
			System.out.println("File is NULL !!!");
			return;
		}
		
		if(file.isDirectory() && file.canRead()) {
			files.clear();

			for(File f: Arrays.asList(file.listFiles())) {				
				final FileTreeNode node = new FileTreeNode(f);
				files.add(node);
				
				node.scan();
			}
		}
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return files.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return files.size();
	}

	@Override
	public TreeNode getParent() {
		TreeNode treeNode = new FileTreeNode(file.getParentFile());
		return treeNode;
	}

	@Override
	public int getIndex(TreeNode node) {
		return files.indexOf(((FileTreeNode)node).getFile());
	}

	@Override
	public boolean getAllowsChildren() {
		return file.isDirectory();
	}

	@Override
	public boolean isLeaf() {
		return !file.isDirectory();
	}

	@Override
	public Enumeration children() {
		return Collections.enumeration(files);
	}

	@Override
	public String toString() {
		return file.getName();
	}
}
